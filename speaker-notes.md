# title slide
  
# Introduction

High energy physics is a data-intensive science.

300 trillion events recorded, processed, filtered and analyzed to find about
100 events which appear to contain a Higgs boson, as was published in 2012,
and leading to the Nobel prize in Physics in 2013 for Higgs and Englert.

Future experiments will yield signficantly larget data sets.

In addition to being data-intensive, much of the analysis work consists of
compute-intensive statistical calculations.

The nature of HEP data is such that *data from each collision (event) is
statistically independent from each other event* -- so parallel processing
is natural.

The HEP community has not traditionally made use of HPC resources, but doing so
is becoming more imperative.

So the question my colleagues and I are addressing is: *Can the Python
ecosystem, and HPC resources, benefit HEP's statistical analyses, and reduce the
time it takes for physicists to do their work?*

# A physics use case: search for Dark Matter

We had to pick a specific scientific use-case in order to address our question.
We chose a *search for dark matter*. What is dark matter?

Astronomers have observed many galaxies and have measured the speed at which
stars rotate around the center of those galaxies. What they found was
unexpected.

This picture shows a fairly typical galaxy, along with measured and predicted
values of the rotational velocity of the stars.

The white line shows how the measured speed varies as the distance from the
center of the galaxy.

The red line shows the calculation based on the force of gravity that holds the
galaxy together and based on the mass of visible stars.

The two don’t match. To make the calculation match the observation, there would
have to be a lot of mass we do not see. That is dark matter.

The dark matter would have to be made up of something. But it can not just be
made of the kinds of particles we know about, or it would be just like normal
matter, and be visible.

The most promising possibility is that Dark matter is previously unobserved
type of particle that interacts very weakly.

The CMS experiment at CERN, and others are searching for direct evidence of
these type of particles.

# The Large Hadron Collider (LHC) at CERN

CERN is a large physics laboratory on the border between France and Switzerland.

The LHC is the highest-energy particle collider in the world, and is located at
CERN.

It collides beams of protons (hydrogen nuclei) at energies of 13 trillion
electron-volts. The beams of protons are brought to collision in four points
around the almost 17 mile circumference ring.

One of those collision points houses the CMS detector.

# The Compact Muon Solenoid (CMS) detector at the LHC

CMS is a particle detector that is designed to see a wide range of particles
produced in high-energy collisions in the LHC. Like a cylindrical onion,
different layers of detectors measure the different particles, and use this key
data to build up a picture of events at the heart of the collision.

Scientists then use this data to search for new phenomena that will
help to answer questions such as: What is the Universe really made of
and what forces act within it?

CMS will also measure the properties of previously discovered particles with
unprecedented precision, and be on the lookout for completely new phenomena.

It took more than 2000 scientists and engineers more than 20 years to design
and build.

Is 100 meters underground at LHC.

This very large device is used to observe very tiny particles

# A particle collision in the CMS detector

This picture is an display of some of the data from a single collision, showing
what particles have been identified.

One collision like this is called an event.

The detector does not directly identify particles. Rather, the paths, energy,
momenta, and identify of particles is *inferred* from things like energy
deposits or ionization patterns in the layers of the detector.

In this picture, light green lines show electron candidates; red lines show
muon candidates; dark green lines show charged particles without a specific
particle type identification.

The inference process can be quite complicated.

# How particles are detected

This shows a small radial section of the full detector, starting from the
collision point (in the circle at the left) and going outward from there.

They key at the left shows the line colors used to indicated different types of
particles. The picture shows how different types of particles typically leave
traces in the different parts of the detector, and thus how we infer from what
is seen in the dector what kinds of particles were likely to have caused the
detector signals we observe.

Focus first on the muon, the long blue trace. This particle makes it through
the outer part of the detector, and leaves observable deposits in the outermost
layers. It is the only type of particle that does so, and so is easily
identifiable. By measuring how much the track bends, we can infer the momentum
of the particle, and by observing the direction of the ben, we can tell the
charge, to distinguish a negative-charge muon from a positive-charge anti-muon.
Yes, HEP physicists regularly observe antimatter.

Next look at the red line. It shows a charged particle that is absorbed in the
inner part of the detector. This is identified as an electron.

The amount of the curvature is direct measurement of the momentum. And along
with the particle ID it tells how much energy this particle has.

The inferred types and momenta of these particles are what is used in most HEP
analysis tasks.

Particle identification like this is key to reducing the size of the data sets
that are processed in analysis.

# Statistical analysis: a search for new particles

This plot shows the end result of a prototypical physics analysis; in this
case, the discovery of a new particle with a mass of 125 GeV (about the mass of
an iodine atom). This is the Higgs boson.

In this case, we look for events with at least 2 high-momentum electrons and 2
muons -- collectively leptons. From their momenta, and assuming all the
particles came from the production and decay of a single particle, we can
calculate what the mass of that particle would be.

The colored areas show the two main contributions of "backgrounds" -- known
physics processes that can mimic the signal we are looking for. Show in red is
the prediction of what production of a 125 GeV mass Higgs boson would look
like. And finally the dots show the actual data observed by CMS -- and it fits
the predictions of the 125 GeV Higgs boson. This is how the Higgs boson was
discovered.

This analysis shows the features we are interested in: 300 trillion events
observed to yield the data shows, *many* more events simulated to produce the
predictions. All the events are statistically independent, so parallel
processing is natural. And the calculations involved are computationally
expensive. It is a natural environment in which to explore big data and HPC
technologies.

# The current computing solution

In HEP, the traditional (and current) computing solution takes advantage of the
statistical independence of events by putting data in many files (often
millions of them), and batch-processing those files separately, on distributed
computing farms.

Processing generally consists of multiple steps, each step reducing the data
size by a couple of orders of magnitude.

These many steps are time-consuming and difficult, so a great deal of effort
has gone into automating parts of them.

Because early steps are expensive to repeat, they are repeated rarely.

Traditionally little use of parallel programming has been made; recently this
has changed, but most physicists are not expert in parallel programming. So
analysis tasks do not take much advantage of this.

# Why the Python-HPC ecosystem might be attractive

HEP is being driven for cost reasons to make more use of HPC systems, rather
than relying upon the traditional HEP computing model.

We want to find the technologies that makes the move to HPC easiest for
physicists, most of whom do not wish to spend much of their time learning
complex programming technologies that require significant expertise.

The Python programming language is already popular in HEP.

The combination of *numpy*, *pandas*, *mpi4py* and *h5py* present a combination
of capabilities that seemed like they might meet the needs of HEP scientits.

This is what we investigated, concentrating on the analysis tasks shown as the
last 3 steps in the previous diagram.

# Overview: computing solution using Python and HDF5

The high-level view of the computing solution we investigated is this:

  * Read HDF5 file into multiple DataFrames, one per particle candidate type.
  * Define vectorized filtering operations on a DataFrame (as opposed to
    writing loops over events).
  * Data are loaded once in memory and may be processed several times.
  * Make plots, repeat as needed.

Let's take a look first at the data organization in our files, and then how
we process those data.

# HDF5: salient features

HDF5 is a complex and rich library; we highly here only a few of its features,
and those of related software, important for our use.

  * Data are representable as columns (datasets) in tables (groups).
  * HDF5 is a widely-used format for the HPC systems; this allows us to
    use traditional HPC technologies to process these files.
  * Efficient reading of slices of datasets is  supported. This will be critical
    for our use of MPI.
  * Columns can contain multi-dimensional arrays; we need these for some
    applications (but not for this use case).
  * Because pandas DataFrames can not hold multi-dimensional arrays, we
    do not use native pandas support for HDF5. This was a bit of a
    disappointment.
  * We used h5py for simple and efficient reading of datasets into *ndarrays*.
  
Because the HDF5 datasets carry the dimensionality of the arrays, we can write
*generic code* that reads any of our tables correctly, without requiring
knowledge of the format of the table.

This kind of ease-of-use is critical for our science community.

# Tabular data storage using HDF5

The diagram on the left shows the organization of data the HEP has
traditionally used: an HEP-custom data format (ROOT), and non-tabular storage.
In this format, each entry represents an event, and contains variable-sized
arrays of data; different numbers of each type of particle in each event.

The diagram on the right shows the same data put into a tabular organization.
In HDF5, each of these tables is represented by an HDF5 group, and each column
is represented by an HDF5 dataset in a group.

This data format is suitably general, and lends itself to efficient use in
analysis.

# Applicability of pandas and MPI

Recall our primary aims are *ease of use* (esepcially to provide parallel
programming requiring little or no expertise) and *speed of processing* (so we
need to take advantage of parallelism). Here's how pandas and MPI help us.

  * For this use case, the arrays we read contain only scalar values (no
    multi-dimensional arrays).
  * We can thus efficiently turn the numpy *ndarrays* into pandas *DataFrames*.
  * Different tables can be processed independently; we only need to read
    the data we care about for our task. This is critical because each analysis
    typically only reads a small portion of the data for each event.
  * We can use high-level pandas facilities:  column-oriented transformations and reductions can be executed in parallel.
  * Each MPI rank reads part of each column; each rank forms its own pandas
    DataFrame. The MPI ranks share no data between them.
  * Transformations and filtering are also done independently for each rank.
  * Reduction steps needed only to calculate global quantities.

# Reading tabular data into pandas Dataframes

This diagram shows how we map a table, represented on disk as an HDF group, into a memory as a DataFrame.

The diagram shows 3 MPI ranks reading a table with 4 columns.

Each rank reads a contiguous portion of every column. Each column is
represented in memory as a numpy ndarray. In each rank, the arrays are bound
into a DataFrame. This requires no copying of data, and no communication
between ranks.

The result is each rank having a DataFrame, with the DataFrames being of very
nearly equal size. None of the DataFrame data is shared between ranks.

# An example analysis

This shows a simplified version of a part of a real analysis. It has the
important typical features:

  * the use of some, but not all, of the columns in a table
  * filtering of data, selecting those that are interesting for further
    processing
  * calculations of varing complexities
  * use of more than one table (here, *electrons* and *global event features*)

Initially it would seem that relational databases would meet our needs, since
we're processing tabular data. But there are features of what we do that are
not dealt with easily in SQL; databases we are familiar with are unable to deal
with the kinds of queries we do in an efficient manner.

# Electron filtering with the pandas API

Note that this is a simplied version of real filter code; the real version uses
more calculated variables (it uses about 20 columns).

The critical features are:

  * simplicity: the high-level API eliminates writing loops; operations are
    performed on arrays of data transparently.
  * this code is inherently parallel!
  * no communication is required

Because no data are shared between ranks, filtering is automatically and
perfectly parallelized. The physicist programmer *does not need to write
anything* to make this parallel.

# Histogramming electron pt with the pandas API

This is a code fragment from the analysis.

Again, the high-level interface of numpy makes the programming simple.

However, unlike the previous code, some MPI expertise is required. The
physicist programmer has to split the histogramming into two steps: the
local-to-the-rank portion, and the reduction necessary to make a single
histogram for output. While mpi4py makes this reduction a single function call,
it is up to the physicist programmer to get this right.

# Performance measurements

Recall we are interested in both *speed* and *scalability* of the code.

  * Our data sample was 360~million events, with 180 million electrons.
    (This was 60~GiB of data read from a 500 GiB file.)
  * We ran on Cori Phase I (Haswell) and Phase II (KNL), varying the number of
    nodes used.
  * On the KNL nodes, we used both *flexible memory modes*: flat and cache, for
    access to 16 GB of on-package high-bandwidth memory. *Flat* mode means that
    the memory is available as a distinct NUMA node, addressible as such: we
    did not directly write any code that did so. So we expect no benefit from
    this mode. *Cache* mode makes the memory available as if it were L3 cache.
  * We ran identically configured jobs 3 times, to evaluate reproducibility.
    (For one of the jobs, we ran out of available time and did only one run).
  * We time separately the *read* and *analysis* steps.
  * In every case, we use all physical cores, with an MPI rank for each core.

# Scaling of read performance

This plot shows the reading speed, in millions of records per second, for each
platform (and two memory modes for KNL), as a function of the number of nodes
running.

In this measurement, we recorded the reading speed for rank 0 of each job.

The maximum reading speed achieved on the Haswell nodes is much greater than
that achieved on the KNL nodes. 

On the KNL nodre, *flat* mode seems to do better than *cache* mode.

The main feature of this plot is the wild variability of the measured
performance of repeated identical configurations, especially on the Haswell
nodes.

# Scaling of analysis performance

This plot shows the in-memory analysis processing speed, in millions of records
per second, for each configuration we ran.

Again, we recorded the speed for rank 0 of each job.

It appears that the *cache* mode does somewhat better than the *flat* mode, but
from this data the difference is not dramatic. The Haswell (Cori I) performance
looks a lot like the KNL *cache* mode performance.

Again, there is wide variability between repeated identical configurations,
especially on the Haswell nodes.

# Further performance studies

After observing the dramatic variations in performance, we went back and added
more detailed measurements, and additional control over the processing.

  * We controlled process migration by binding each rank to a specific core.
  * We explored whether lack of coordination between ranks was causing
    variations in read performance, using three strategies for placing MPI barriers
    
    * *nobarrier*, which is identical to the initial scheme;
    * *1barrier*, which uses a barrier before the read step, to
      ensure all ranks begin reading at the same time; and
    * *2barrier*, which also uses a barrier after the read, to ensure
        all ranks begin processing at the same time.

In addition...

# Further performance studies (cont'd)

  * We ran multiple tests in a single batch job, to evaluate the 
    short-timescale reproducibility of reading performance.
  * We ran two sets of batch jobs for each configuration, to evaluate the
    longer-timescale reproducibility.
  * We ran 8- and 32-node jobs, because the 8-node jobs were the largest
    that seemed to have modest variation in speed, and 32-node jobs showed
    very dramatic variations.
  * Note: 8-node jobs have 256 MPI ranks; 32-node jobs have 1024 ranks.
  
This time, we instrumented every MPI rank of each program.

# Read speeds for each rank, 8 node runs, Haswell

This kind of plot is called a *violin plot*. It shows the distribution of
reading speeds as estimated by a Gaussian kernel density estimate. Each
*violin* shows a density estimate derived from 256 measurements (one from each
rank.)

This plot shows the distribution of reading speeds for each of 8 program runs
in the same batch job, for the three different synchronization strategies
(shown by the tan panel labels), and for two repetitions of
identically-configured batch jobs (shown by the green panel labels).

All runs in a given batch job were completed within several minutes of each
other.

The batch jobs were separated by many hours.

Naive expectation would be that all runs in the same column would be similar;
this is not what we observed.

Furthermore, each of the distributions within a single job is multi-model, and
thus reporting a single mean or median would not be a good description for the
data.

Reading speeds vary by about a factor of 20. The 1barrier runs show somewhat
worse performance than the 2- and no-barrier cases. A few of the runs within the
1barrier jobs were notably slower than the others.

# Read speeds for each rank, 32 node runs, Haswell

This plot shows the read speed distributions for the 32-node runs. Here, each
violin represents 1024 measurements, one for each rank.

Again the nobarrier and 2barrier runs are broadly similar. This time, the
dramatic feature is the uniformity of the each individual 1barrier run, and the
spread of performance between them.

We were concerned at first that we had a data handling error and recorded the
same value for each MPI rank. So we took a closer look.

Let's look at batch 1, run 2 ...

# Read speeds for batch 1, run 2, 1barrier strategy, 32 nodes

This is the distribution that showed in the previous violin plot as a
zero-width distribution. The different ranks did in fact have slightly
different speeds, but the distribution within this one job has a spread of
about 1/100 of one percent.

# Merge (join) speeds for each rank, 8 node runs, Haswell

We also looked more closely at the analysis processing speed. We measured
separately the several steps of the analysis: joining of dataframes, filtering
electrons, per-rank histogramming, and reduction to a single histogram. The
most time-consuming part of this is the joining of dataframes, shown here.

This violin plot shows the join speed for each the 8 node runs.

Unlike the reading speeds, these distributions are single-modal, and the range
of variation is much smaller.

A better way to show these data is a box-and-whisker plot ...

# Read speeds for batch 1, run 2, 1barrier strategy, 32 nodes

This plot shows the same data, but better shows the variations. The black dots 
show the median speeds. There is no significant difference between runs in the
same batch job, betweeen different batch jobs with the same synchronization strategy, or between strategies.

50% of the data points fall inside the box that is mostly obscured by the size
of the black dot. The individual outliers are shown, and show no significant
difference between the different runs.

Note the median read speed is about 105 million records/second for each run.

# Merge (join) speeds for each rank, 32 node runs, Haswell

This plot shows the measurements for the 32-node runs. The same gross features
are visible, with a couple of anomalously slow and variable runs in the
nobarrier mode.

Note tha the median read speed here is about 450 million records/second. With 4
times as many cores to use, we process the data at 4 times the rate -- perfect
scaling, as we hoped for in a task that involves no communication between ranks.

# Lessons learned

  * HDF5 provides the facilities we need to represent data in a form that
    makes it easy and efficient to process in memory. Such an organization also
    allows reading and visualization to be simplified.
  * Traditional HEP analysis use many files; for efficient use in HPC
    environments, we need to change this paradigm.
  * Many operations commonly used in HEP analysis tasks can be directly
    described using the high-level APIs provided by numpy and pandas DataFrame
    API. The resulting code is easy to read and efficient as well.

# Lessons learned (cont'd)

  * Because the development environment and tools provided by Python were
    sufficiently easy to use, we were able to quickly identify performance
    bottlenecks. Such ease-of-development for non-experts is a critical
    advantage for our community.
  * Many HEP analysis tasks are very demanding of the I/O system.
    Performance evaluation of such processing is made difficult by the fact
    that the global filesystem is a shared resource. Access to performance data
    for the machine as a whole seems necessary to understand performance.

# Conclusion

  * The processing stages in the CMS Dark Matter analysis represent an
    important class of problems in experimental HEP, but the data set available 
    to us is small, limiting our ability to make strong observations about
    scalability.
    
  * Typical stages include applying several selection and filtering
    criteria and plotting.
    
  * HEP scientists already use Python for several data processing tasks,
    so providing them with data layouts and high-level APIs to enable efficient
    analysis operations in Python is promising.

These results were sufficiently promising, and we are continuing work on this...

# Future work

  * We will expand our investigations to include additional HEP use-cases.
  * We now have access to a data set more than 100 times larger more than 4 TB
    disk, and approximately 30 TB in memory; we will study the reading and
    in-memory processing performance and scalibilty using those data.
  * We will investigate different reading strategies for KNL nodes; it may
    be better to limit the number of ranks that do reading on that platform.
  * We will work on providing more interfaces that remove the
    remaining complexity of parallel code, for example the explicit 
    synchronization and communication for reductions.
  * We have a C++ library that provides facilities for writing HDF5 
    files in the format used in this work, available on BitBucket.

# Acknowledgements

Thank you for your time, and I'm happy to answer any questions.

