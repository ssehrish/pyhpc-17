########################################################################
# This GNUmakefile requires UPS product ssddoc v1_09 or better for
# knitting and font embedding: setup the UPS product or ensure an
# up-to-date copy of the cet-is repository in parent.
#
# To make a PDF file pyhpc-17-final.pdf with embedded fonts, simply:
#
#   make final
#
# To make the .tex file for debugging purposes, either make it
# explicitly:
#
#   make pyhpc-17.tex
#
# OR uncomment the definition of the .SECONDARY special target below.
# You may then wish to add pyhpc-17.tex to the clean_files variable
#
# 2017-10-24 CG.
########################################################################
TALK=Python_and_HPC_for_High_Energy_Physics_Data_Analyses

# Things we wish to build. Note that we do *not* mention $(TALK).pdf
# here because it is not *built* in the normal way.
PRODUCTS = pyhpc-17.pdf pyhpc-17-talk.pdf

# Build $(TALK).pdf as part of the all target.
ALL = $(PRODUCTS) $(TALK).pdf

####################################
# Preamble.

# Need to find latex.mk somewhere.
ifneq (,$(SSDDOC_INC))
  ldir = $(SSDDOC_INC)
else
  ldir = ../cet-is/ssddoc/include
endif

# Include it.
include $(ldir)/latex.mk

# Check version.
ifneq (,$(LATEX_MK_VERSION_NUM))
  latex_mk_version_ok := $(shell (($(LATEX_MK_VERSION_NUM) >= 10900)) && echo OK)
endif

ifeq (,$(latex_mk_version_ok))
  $(error "ssdoc >= v1_09_00 required.")
endif
####################################

# On with the show.
export TEXINPUTS=$(LATEX_MK_DIR)//:

# Instructions to generate $(TALK).pdf as a final document for
# submission with embedded fonts and a nice filename.
.INTERMEDIATE : pyhpc-17-talk-final.pdf
$(TALK).pdf : pyhpc-17-talk-final.pdf
	$(AT)mv -v $(<) $(@) $(SILENT)

override clobber_files += $(TALK).pdf

# Uncomment to keep the generated .tex file around (until we clean or clobber).
#.SECONDARY: pyhpc-17.tex
#override clean_files += pyhpc-17.tex

# Regenerate and clean up .bbl files.
override LATEXMK_EXTRA_OPTS += -bibtex
